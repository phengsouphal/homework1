package com.example.gamer.homework;

import java.io.Serializable;

public class Info implements Serializable {

    private String name;
    private String phone;
    private String classroom;


    public Info(String name, String phone, String classroom) {
        this.name = name;
        this.phone = phone;
        this.classroom = classroom;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getClassroom() {
        return classroom;
    }

    public void setClassroom(String classroom) {
        this.classroom = classroom;
    }
}
