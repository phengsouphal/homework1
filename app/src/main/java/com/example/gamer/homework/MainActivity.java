package com.example.gamer.homework;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText Name,Phone,Classroom;
    Button btncall;
    String phonecall;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Name=(EditText)findViewById(R.id.txtname);
        Phone =(EditText)findViewById(R.id.txtphone);
        Classroom=(EditText)findViewById(R.id.txtclass);
        btncall=(Button)findViewById(R.id.btncall);

    }

    public void getDetailActivity(View view) {

        Intent i=new Intent(this,DetailActivity.class);

        String name=Name.getText().toString();
        String phone=Phone.getText().toString();
        String classroom=Classroom.getText().toString();

        Info info = new Info(name,phone,classroom);
        i.putExtra("Student",info);
        startActivityForResult(i,2);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==2&&resultCode==2&&data!=null){
            Info info = (Info) data.getSerializableExtra("Student");

            Name.setText(info.getName());
            Phone.setText(info.getPhone());
            Classroom.setText(info.getClassroom());

        }


    }

    public void makePhoneCall(View view){

        Intent i=new Intent(Intent.ACTION_DIAL);
        String phone=Phone.getText().toString();

    i.setData(Uri.parse("tel:"+phone));
    startActivity(i);
    }

}
