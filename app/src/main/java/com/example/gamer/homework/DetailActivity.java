package com.example.gamer.homework;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    String Name;
    TextView tv_name,tv_phone,tv_class;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        tv_name= findViewById(R.id.textname);
        tv_phone= findViewById(R.id.textphone);
        tv_class= findViewById(R.id.textclass);

        Info info=(Info) getIntent().getSerializableExtra("Student");

        tv_name.setText(info.getName());
        tv_phone.setText(info.getPhone());
        tv_class.setText(info.getClassroom());

    }

    public void btnback(View view){

        Intent i=new Intent();

        String name = tv_name.getText().toString();
        String phone = tv_phone.getText().toString();
        String classroom = tv_class.getText().toString();


        Info info=new Info(name,phone,classroom);
        i.putExtra("Student",info);
        setResult(2,i);
        finish();
    }
}
